# EP2 - Jogo do Cobrão - OO 2019.2 (UnB - Gama)

* **Aluno:** Mateus Gomes do Nascimento
* **Matrícula:** 18/0106821

## Descrição

Neste trabalho foi criado um Snake Game (vulgo jogo da cobrinha), no qual o jogador controla uma cobrinha que têm um único e exclusivo objetivo em sua vida,  **ficar fibrada**. Para atingir este sonho que nossa heroína cobrinha cultivou desde sua infância modesta no interior de Taguatinga, o jogador deve ir atrás das frutas que melhoram sua pontuação (as frutas serão melhor explicadas mais tarde). O jogo conta com três tipos de snakes e quatro tipos de frutas, frutas estas que são sempre geradas em uma localização aleatória do cenário e sempre em duplas (uma aleatória e uma comum), e cabe ao jogador escolher qual fruta merece mais prioridade de ser comida. Além disso, o cenário conta com paredes em lugares específicos e não aleatórios que podem atrapalhar a jornada da nossa heroína cobra dependendo do tipo escolhido.

**OBS:** Diagrama de classes e Diagrama de Casos de Uso dentro da pasta "UML"

## Tipos de cobra
Como dito anteriormente, o jogo conta com três tipos de cobra, e o jogador escolhe qual tipo ele vai utilizar antes de começar o jogo.

* **Tipo Comum:** Esta cobra é apenas um sujeito comum num mundo injusto onde outras cobras nascem privilegiadas. Esta cobra não possui habilidades especiais e ela morre se entrar em contado com seu próprio corpo, com as paredes do cenário, ou com a fruta do tipo bomb, mas caso esta cobra entre em contato com os limites da tela (bordas), ao invés de morrer, ela ressurge do outro lado da tela;

* **Tipo Kitty:** Poder especial: **raiva**. Esta cobra por não estar nem um pouco feliz, consegue atravessar as paredes espalhadas pelo cenário utilizando a força do ódio, sem levar dano algum, mas sua raiva não é implacável, pois a mesma morre se entrar em contato com seu próprio corpo, com os limites do cenário (bordas) ou com a fruta do tipo bomb;

* **Tipo Star:** Esta cobra é recomendada para jogadores mais casuais, ela recebe pontuação dobrada das frutas e possui os mesmos casos de morte que a cobra Comum.

## Tipos de frutas

As frutas, como dito na descrição, são geradas de maneira aleatória e de duas em duas, e além disso, depois de 7 segundos, se a cobra não comer nenhuma fruta, as que estavam presentes no cenário somem e outras duas são geradas.

* **Tipo Simple (Cor Rosa Claro):** Esta fruta é plantada sem uso de agrotóxicos, com isso o valor de pontuação dela é 1 (que pode ser dobrado pela cobra star) e aumenta o tamanho da cobra em 1 unidade;

* **Tipo Big (Cor Verde):** Esta fruta com ajuda dos agrotóxicos, possui o valor de pontuação base igual a 2, mas aumenta o tamanho da cobra exatamente como a fruta Simple (mas aumenta as chances da cobra desenvolver câncer);

* **Tipo Decrease (Cor Roxa):** Esta fruta não dá pontuação, porém, ela volta a cobra para seu tamanho inicial, mantendo os pontos adquiridos até então;

* **Tipo Bomb (Cor Cinza):** Após muitas mutações genéticas em frutas feitas pelos ancestrais de nossa heroína, um novo tipo de fruta surgiu, repleta de pólvora em seu interior esta fruta pode ser facilmente classificada como uma arma, e se a cobra tentar comer a mesma, é morte instantânea.

## Como jogar

* **Menu:** Ao iniciar o jogo, é apresentado ao jogador um menu com duas opções, a primeira é jogar, que leva o jogador a um menu para escolher o tipo de cobra e depois iniciar de fato o jogo, e a outra opção é a de tutorial, e lá é mostrado de maneira visual como controlar a cobra e o que cada cobra/fruta faz de especial. O menu de tutorial conta também com uma opção de "voltar", que leva o jogador de volta ao menu inicial;

* **Movimento:** Para movimentar a cobra o jogador deve utilizar as setinhas do teclado;

* **Reiniciar:** Assim quando a cobra morrer, é exibido uma mensagem de Game Over, e caso o jogador pressione a tecla espaço o jogo volta para a opção de escolha de cobra.

## Lista de dependências

Este jogo foi desenvolvido em java, na versão "Java SE JDK 11.0.4", utilizando a IDE Eclipse dentro do sistema operacional Ubuntu 19.4.

Para iniciar o jogo, o jeito recomendado é, com a Java Virtual Machine (JVM) instalada, executar o arquivo "Jogo_do_Cobrao.jar" que está contido na pasta principal do repositório. Para executar no linux basta digitar o seguinte no terminal:

`` java -jar Jogo_do_Cobrao.jar ``

O jeito alternativo é abrindo o projeto pelo Eclipse ou por outra IDE e compilar a classe "Menu.java".