import java.awt.Color;
import java.awt.Graphics;

//Classe responsável por criar as paredes espalhadas pelo cenário
public class Parede {
	private int xCoor, yCoor, WIDTH, HEIGHT, points;
	private String tipo;
	
public Parede(int xCoor, int yCoor, int tileSize, String tipo) {
			
	this.xCoor = xCoor;
	this.yCoor = yCoor;
	this.HEIGHT = tileSize;
	this.WIDTH = tileSize;
	this.tipo = tipo;	
		
	if(tipo == "Parede") {
		this.points = 0;
	}
		
	}
	public String getTipo() {
		return tipo;
	}

	public int getpoints() {
		return points;
	}
	
	public void tick() {
			
	}
		
	public void draw(Graphics g) {
		//Define a cor e desenha a parede
		if(tipo == "Parede") {
			g.setColor(Color.GREEN);
			g.fillRect(xCoor*WIDTH, yCoor*HEIGHT, WIDTH, HEIGHT);
		}
	}
	
	//Metodos acessores
	public int getxCoor() {
		return xCoor;
	}
	
	public int getyCoor() {
		return yCoor;
	}
	
	
}

