import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JPanel;

//Classe responsável pela Engine do jogo
public class Gamepanel extends JPanel implements Runnable, KeyListener{

	private static final long serialVersionUID = 1L;
	
	//Tamanho da tela
	public static final int WIDTH = 600, HEIGHT = 600;
	
	private Thread thread;
	
	//Variável para saber se o jogo está rodando
	private boolean running;
	
	//Variáveis de movimentação
	private boolean right = true, left = false, up = false, down = false;
	
	//Variáveis relacionadas à cobra
	private Body body;
	private ArrayList<Body> snake;
	private String tipoCobra;
	
	//Variáveis relacionadas à fruta
	private Fruit fruta;
	private ArrayList<Fruit> frutas;
	private long time;
	
	//Variável para randomizar localização das frutas
	private Random r;
	
	//Variáveis para movimentação e tamanho da cobra
	private int xCoor = 10, yCoor = 10, size = 5;
	private int ticks = 0;
	
	//Variável para armazenar pontuação
	private int points = 0;
	
	//Variáveis para as paredes do cenário
	private Parede parede;
	private ArrayList<Parede> paredes;

	
	
	public Gamepanel(String tipoCobra) {
		setFocusable(true);
		
		//Cria o tamanho da janela
		setPreferredSize(new Dimension(WIDTH, HEIGHT));
		addKeyListener(this);
		
		//Inicializa ArrayLists
		snake = new ArrayList<Body>();
		frutas = new ArrayList<Fruit>();
		paredes = new ArrayList<Parede>();
		
		//Cria uma variável random
		r = new Random();
		this.tipoCobra = tipoCobra;
		
		//Inicia o jogo
		start();
	}
	
	//Método para iniciar o jogo
	public void start() {
		running = true;
		thread = new Thread(this);
		thread.start();
	}
	
	//Método para parar o jogo
	public void stop() {
		running = false;
		try {
			thread.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		repaint();
		
	}
	public void tick() {
		//Criação do corpo da cobra
		if(snake.size()==0) {
			body = new Body(xCoor, yCoor, 10, tipoCobra);
			snake.add(body);
		}
		ticks++;
		
		//Controla movimentação e a velocidade da cobra
		if(ticks>500000) {
			if(right) xCoor++;
			if(left) xCoor--;
			if(up) yCoor--;
			if(down) yCoor++;
			
			ticks = 0;
			
			body = new Body(xCoor, yCoor, 10, tipoCobra);
			snake.add(body);
			
			if(snake.size()>this.size) {
				snake.remove(0);
			}
		}
		
		if(frutas.size()==0) {
			//Randomiza a posição das frutas
			int test=0;
			
			int xCoor1 = r.nextInt(59);
			int yCoor1 = r.nextInt(59);
			
			int xCoor2 = r.nextInt(59);
			int yCoor2 = r.nextInt(59);
			
			for(int i=0; i<paredes.size(); i++) {
				if(xCoor1 == paredes.get(i).getxCoor() && yCoor1 == paredes.get(i).getyCoor()) {
					test++;
				}
				if(xCoor2 == paredes.get(i).getxCoor() && yCoor2 == paredes.get(i).getyCoor()) {
					test++;
				}
			}
			
			for(int i=0;i<snake.size();i++) {
				if(xCoor2 == snake.get(i).getxCoor() && yCoor2 == snake.get(i).getyCoor()) {
					test++;
				}
				if(xCoor2 == snake.get(i).getxCoor() && yCoor2 == snake.get(i).getyCoor()) {
					test++;
				}
			}

			if(test == 0) {
				fruta = new Fruit(xCoor1, yCoor1, 10, "Simple");
				frutas.add(fruta);
				
				fruta = new Fruit(xCoor2, yCoor2, 10);
				frutas.add(fruta);
				frutas.add(fruta);
				
				time = System.currentTimeMillis();
			}
		
		}
		//Faz as frutas reaparecerem depois de certo tempo (7 segundos)
		if(time+10000 == System.currentTimeMillis()) {
			frutas.removeAll(frutas);
		}

		for(int i=0;i<frutas.size();i++) {
			
			//Reação da cobra ao comer cada fruta
			if(xCoor == frutas.get(i).getxCoor() && yCoor == frutas.get(i).getyCoor()) {
				
				//Fruta Simple, aumenta o tamanho da cobra
				if(frutas.get(i).getFruit()=="Simple") {
					size++;
				}
				//Fruta Big, aumenta o tamanho da cobra
				else if(frutas.get(i).getFruit()=="Big") {
					size++;
				}
				//Fruta Decrease, volta a cobra para o tamanho inicial
				else if(frutas.get(i).getFruit()=="Decrease") {
					size = 5;
					while(snake.size()!=5) {
						snake.remove(0);
					}
				}
				//Fruta Bomb, mata a cobra
				else if(frutas.get(i).getFruit()=="Bomb") {
					stop();
				}
				
				//Pontuação que cada cobra recebe
				if(body.getTipoCobra()=="Star") {
					//Cobra Star recebe o dobro de pontuação
					this.points += (frutas.get(i).getValorFruta()*2);
				}else if(body.getTipoCobra()=="Comum" || body.getTipoCobra()=="Kitty") {
					//Outras cobras recebem pontuação normal
					this.points += frutas.get(i).getValorFruta();
				}

				frutas.removeAll(frutas);
				i++;

			}
		}
		//Colisão da cobra com seu próprio corpo
		for(int i=0;i<snake.size();i++) {
			if(xCoor==snake.get(i).getxCoor() && yCoor == snake.get(i).getyCoor()) {
				if(i!=snake.size()-1) {
					stop();
				}
			}
		}
		//Cobra Star atravessa as bordas e aparece do outro lado do cenário
		if(body.getTipoCobra()=="Comum"||body.getTipoCobra()=="Star") {
			if(xCoor < 0) {
				xCoor = 59;
			}
			if(xCoor > 59) {
				xCoor = 0;
			}
			if(yCoor < 0) {
				yCoor = 59;
			}
			if(yCoor > 59) {
				yCoor = 0;
			}
		}else {
			//Colisão da cobra Comum e kitty com a borda, gerando o fim de jogo
			if(xCoor < 0 || xCoor >59 ||yCoor < 0 || yCoor >59) {
				stop();
			}
		}

		
	}
	public void paint(Graphics g) {
		
		//Cria cenário
		g.clearRect(0, 0, WIDTH, HEIGHT);
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, WIDTH, HEIGHT);
		
		//Cria paredes pelo cenário
		for(int i=10; i<30; i++) {
			parede = new Parede(30,i,10,"Parede");
			paredes.add(parede);
			parede = new Parede(50,i,10,"Parede");
			paredes.add(parede);
			parede = new Parede(i-10,80,10,"Parede");
			paredes.add(parede);
			parede = new Parede(i,45,10,"Parede");
			paredes.add(parede);
		}
		for(int i=0; i<paredes.size();i++){
			paredes.get(i).draw(g);
		}
		//Colisão com as paredes nas cobras Star e Comum resulta no fim de jogo
		for(int i=0; i<paredes.size(); i++) {
			if(body.getTipoCobra() != "Kitty") {
				if(xCoor == paredes.get(i).getxCoor() && yCoor == paredes.get(i).getyCoor()) {
					stop();
				}
			}
		}

		//Desenha cobra
		for(int i=0;i<snake.size();i++) {
			snake.get(i).draw(g);
		}
		
		//Desenha fruta
		for(int i=0;i<frutas.size();i++) {
			frutas.get(i).draw(g);
		}
		
		//Desenha na tela a pontuação do jogador
		g.setColor(Color.YELLOW);
		g.setFont(new Font("arial", Font.BOLD, 17));
		g.drawString("Pontuação: "+ points, 5, 15);
		
		//Quando a cobra morre:
		if(running == false) {
			
			//Desenha "FIM DE JOGO" em vermelho
			g.setColor(Color.RED);
			g.setFont(new Font("arial", Font.BOLD, 50));
			g.drawString("FIM DE JOGO", 120, 350);
			
			//Mostra a pontuação final do jogador em amarelo
			g.setColor(Color.YELLOW);
			g.setFont(new Font("arial", Font.BOLD, 30));
			g.drawString("Pontuação final: "+ points, 140, 390);
			
			//Desenha a mensagem para recomeçar o jogo
			g.setColor(Color.WHITE);
			g.setFont(new Font("arial", Font.BOLD, 20));
			g.drawString("Aperte espaço para jogar novamente", 90, 420);
			
			g.dispose();
			g.clearRect(10, 10, WIDTH, HEIGHT);
		}

		

	}

	@Override
	public void run() {
		while(running) {
			tick();
			repaint();
		}
		
	}

	@Override
	//Método que controla o que cada tecla do teclado faz
	public void keyPressed(KeyEvent e) {
		int key = e.getKeyCode();
		//Teclas de movimentação
		if(key == KeyEvent.VK_RIGHT && !left) {
			right = true;
			up = false;
			down = false;
		}
		if(key == KeyEvent.VK_LEFT && !right) {
			left = true;
			up = false;
			down = false;
		}
		if(key == KeyEvent.VK_UP && !down) {
			up = true;
			left = false;
			right = false;
		}
		if(key == KeyEvent.VK_DOWN && !up) {
			down = true;
			left = false;
			right = false;
		}
		
		//A tecla espaço reinicia o jogo voltando a tela de escolha de personagem
		if(e.getKeyCode() == KeyEvent.VK_SPACE) {
			points= 0;
			new Jogar();
		}

		
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
}
