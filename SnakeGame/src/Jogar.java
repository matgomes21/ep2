import java.awt.EventQueue;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

//Classe responsável pelo menu de escolha de personagem quando a opção "Jogar" do menu inicial é escolhida
public class Jogar {

	private JFrame frame;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Jogar window = new Jogar();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public Jogar() {
		initialize();
	}

	private void initialize() {
		//Inicializa o frame
		frame = new JFrame();
		frame.setBounds(100, 100, 600, 430);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setLocationRelativeTo(null);
		
		//Cria um botão para a opção de cobra Comum
		JButton btnComum = new JButton("Comum");
		btnComum.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new Main("Comum");
				frame.setVisible(false);
			}
		});
		btnComum.setBounds(30, 110, 150, 45);
		frame.getContentPane().add(btnComum);
		
		//Cria um botção para a opção de cobra Kitty
		JButton btnKitty = new JButton("Kitty");
		btnKitty.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new Main("Kitty");
				frame.setVisible(false);
			}
		});
		btnKitty.setBounds(220, 110, 150, 45);
		frame.getContentPane().add(btnKitty);
		
		//Cria um botão para a opção de cobra Star
		JButton btnStar = new JButton("Star");
		btnStar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new Main("Star");
				frame.setVisible(false);
			}
		});
		btnStar.setBounds(410, 110, 150, 45);
		frame.getContentPane().add(btnStar);
		frame.setVisible(true);
		
		//Adiciona a imagem de background que está localizada em SnakeGame/src
		JLabel lblNewLabel = new JLabel("");
		Image img = new ImageIcon(this.getClass().getResource("jogar.png")).getImage();
		lblNewLabel.setIcon(new ImageIcon(img));
		lblNewLabel.setBounds(0, 0, 600, 400);
		frame.getContentPane().add(lblNewLabel);
		
	}
	
}
