import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Random;

//Classe destinada aos tipos de frutas
public class Fruit {

	private int xCoor, yCoor, width, height;
	
	//Variável que armazena o valor de pontuação que cada fruta têm
	private int valorFruta;
	
	//Variável que armazena o tipo da fruta
	private String fruit;

	//Construtor sem a variável de tipo da fruta
	public Fruit(int xCoor, int yCoor, int tileSize) {
		this.xCoor = xCoor;
		this.yCoor = yCoor;
		width = tileSize;
		height = tileSize;
		
		//Array com os tipos de fruta que pode-se ter
		ArrayList<String> fruta = new ArrayList<String>();
		fruta.add("Simple");
		fruta.add("Bomb");
		fruta.add("Big");
		fruta.add("Decrease");
		
		//Randomiza o tipo de fruta
		Random rand = new Random();
		int r = rand.nextInt(4);
		this.fruit = fruta.get(r);
		
		//Pontuação de cada tipo de fruta
		if(fruit == "Simple") {
			this.valorFruta = 1;
		}
		else if(fruit == "Bomb") {
			this.valorFruta = 0;
		}
		else if(fruit == "Big") {
			this.valorFruta = 2;
		}
		else if(fruit == "Decrease") {
			this.valorFruta = 0;
		}
	}
	
	//Construtor com a variável que armazena o tipo da fruta
	public Fruit(int xCoor, int yCoor, int tileSize,String fruit) {

		this.xCoor = xCoor;
		this.yCoor = yCoor;
		this.height = tileSize;
		this.width = tileSize;
		
		//A fruta recebe o tipo de fruta dado no construtor
		this.fruit = fruit;		
		
		//Pontuação de cada tipo de fruta
		if(fruit == "Simple") {
			this.valorFruta = 1;
		}
		else if(fruit == "Bomb") {
			this.valorFruta = 0;
		}
		else if(fruit == "Big") {
			this.valorFruta = 2;
		}
		else if(fruit == "Decrease") {
			this.valorFruta = 0;
		}
		else if(fruit == "Parede") {
			this.valorFruta = 0;
		}
		
	}

	
	public void tick() {
		
	}
	
	//Método para desenhar as frutas
	public void draw(Graphics g) {
		if(this.fruit == "Simple") {
			g.setColor(Color.PINK);
		}
		else if(this.fruit == "Big") {
			g.setColor(Color.GREEN);
		}
		else if(this.fruit == "Bomb") {
			g.setColor(Color.GRAY);
		}
		else if(this.fruit == "Decrease") {
			g.setColor(Color.MAGENTA);
		}
		g.fillRect(xCoor*width, yCoor*height, width, height);
	}

	//Métodos acessores
	public int getxCoor() {
		return xCoor;
	}

	public void setxCoor(int xCoor) {
		this.xCoor = xCoor;
	}

	public int getyCoor() {
		return yCoor;
	}

	public void setyCoor(int yCoor) {
		this.yCoor = yCoor;
	}
	
	public int getValorFruta() {
		return valorFruta;
	}

	public String getFruit() {
		return fruit;
	}
}