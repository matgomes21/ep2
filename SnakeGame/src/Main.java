import javax.swing.JFrame;

public class Main {
	
	private static String tipoCobra;
	
	public Main(String tipoCobra) {
		
		JFrame frame = new JFrame();
		Gamepanel gamepanel = new Gamepanel(tipoCobra);
		
		frame.add(gamepanel);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("JOGO DO COBRÃO");
		frame.setResizable(false);
		
		frame.pack();
		frame.setVisible(true);
		frame.setLocationRelativeTo(null);
	}
	
	public static void main(String[] args) {
		
		new Main(tipoCobra);
	}

}
