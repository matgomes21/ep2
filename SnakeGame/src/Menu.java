import java.awt.EventQueue;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;

//Classe responsável por criar o Menu Inicial
public class Menu {

	private JFrame frame;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Menu window = new Menu();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public Menu() {
		initialize();
	}

	private void initialize() {
		
		//Inicializa a frame
		frame = new JFrame();
		frame.setBounds(100, 100, 700, 870);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		
		//Cria o botão Jogar para iniciar o jogo e adiciona um listener para este botão
		JButton btnPlay = new JButton("JOGAR");
		btnPlay.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new Jogar();
				frame.setVisible(false);
			}
		});
		//Define as dimensões e localização do botão
		btnPlay.setBounds(220, 400, 250, 45);
		frame.getContentPane().add(btnPlay);
		
		//Cria o botão Tutorial para mostrar como se joga e adiciona um listener para este botão
		JButton btnTutorial = new JButton("TUTORIAL");
		btnTutorial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Tutorial();
				frame.setVisible(false);
			}
		});
		//Define as dimensões e a localização do botão
		btnTutorial.setBounds(220, 470, 250, 45);
		frame.getContentPane().add(btnTutorial);
		
		//Adiciona a imagem de background que está salva na pasta SnakeGame/src
		JLabel lblNewLabel = new JLabel("");
		Image img = new ImageIcon(this.getClass().getResource("menu.png")).getImage();
		lblNewLabel.setIcon(new ImageIcon(img));
		lblNewLabel.setBounds(0, 0, 700, 850);
		frame.getContentPane().add(lblNewLabel);
	}
}
