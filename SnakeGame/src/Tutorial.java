import java.awt.EventQueue;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

//Classe responsável pelo Menu de Tutorial
public class Tutorial {

	private JFrame frame;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Tutorial window = new Tutorial();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public Tutorial() {
		initialize();
	}

	private void initialize() {
		//Inicializa o frame
		frame = new JFrame();
		frame.setBounds(100, 100, 600, 730);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setLocationRelativeTo(null);
		
		//Cria um botão Voltar para retornar ao menu inicial
		JButton btnVoltar = new JButton("Voltar");
		btnVoltar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new Menu();
				frame.setVisible(false);
			}
		});
		//Define as dimensões e a localização do botão
		btnVoltar.setBounds(490, 10, 100, 30);
		frame.getContentPane().add(btnVoltar);
		
		frame.setVisible(true);
		
		//Adiciona a imagem de background, que está localizada em SnakeGame/src
		JLabel lblNewLabel = new JLabel("");
		Image img = new ImageIcon(this.getClass().getResource("tutorial.png")).getImage();
		lblNewLabel.setIcon(new ImageIcon(img));
		lblNewLabel.setBounds(0, 0, 600, 700);
		frame.getContentPane().add(lblNewLabel);
		
	}
	
}
