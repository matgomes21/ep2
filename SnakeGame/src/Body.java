import java.awt.Color;
import java.awt.Graphics;

//Classe destinada ao corpo da cobra e aos seus tipos
public class Body {

	private int xCoor, yCoor, width, height;
	
	//Variável que armazena o tipo de cobra
	private String tipoCobra;
	
	public Body(int xCoor, int yCoor, int tileSize, String tipoCobra) {
		this.xCoor = xCoor;
		this.yCoor = yCoor;
		this.width = tileSize;
		this.height = tileSize;
		this.tipoCobra = tipoCobra;
	}
	
	//Acessores para o tipo da cobra
	public String getTipoCobra() {
		return tipoCobra;
	}

	public void setTipoCobra(String cobra) {
		this.tipoCobra = cobra;
	}

	public void tick() {
		
	}
	//Desenha a cobra de acordo com seu tipo
	public void draw(Graphics g) {
		if(this.tipoCobra == "Comum") {
			g.setColor(Color.BLUE);
		}
		else if(this.tipoCobra == "Kitty") {
			g.setColor(Color.RED);
		}
		else if(this.tipoCobra == "Star") {
			g.setColor(Color.YELLOW);
		}
		g.fillRect(xCoor*width, yCoor*height, width, height);

	}
	
	//Metodos acessores
	public int getxCoor() {
		return xCoor;
	}

	public void setxCoor(int xCoor) {
		this.xCoor = xCoor;
	}

	public int getyCoor() {
		return yCoor;
	}

	public void setyCoor(int yCoor) {
		this.yCoor = yCoor;
	}
}
